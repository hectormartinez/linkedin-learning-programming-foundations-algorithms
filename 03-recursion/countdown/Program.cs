﻿using System;

namespace countdown
{
    class Program
    {
        static void Main(string[] args)
        {
            Countdown(5);
        }

        static void Countdown(int x)
        {
            if (x == 0)
            {
                Console.WriteLine("Done!");
            }
            else
            {
                Console.WriteLine($"{x} ...");
                Countdown(x - 1);
            }
        }
    }
}
