using System;

namespace ordered
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] items = { 6, 8, 19, 20, 23, 41, 49, 53, 56, 87 };
            Console.WriteLine(BinarySearch(23, items));
            Console.WriteLine(BinarySearch(87, items));
            Console.WriteLine(BinarySearch(250, items));
        }

        static int BinarySearch(int value, int[] list)
        {
            int maxIndex = list.GetUpperBound(0);
            int lowerIndex = 0;
            int upperIndex = maxIndex;

            while (lowerIndex <= upperIndex)
            {
                int midPoint = (lowerIndex + upperIndex) / 2;
                if (list[midPoint] == value)
                {
                    return midPoint;
                }

                if (value > list[midPoint])
                {
                    lowerIndex = midPoint + 1;
                }
                else
                {
                    upperIndex = midPoint - 1;
                }
            }
            return -1;
        }
    }
}
