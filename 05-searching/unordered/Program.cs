using System;

namespace unordered
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] items = { 6, 20, 8, 19, 56, 23, 87, 41, 49, 53 };
            Console.WriteLine(FindItem(87, items));
            Console.WriteLine(FindItem(250, items));
        }

        static int FindItem(int value, int[] list)
        {
            for (int i = 0; i < list.GetUpperBound(0); i++)
            {
                if (list[i] == value)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
