﻿using System;

namespace bubble
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 6, 20, 8, 19, 56, 23, 87, 41, 49, 53 };
            Console.Write("Original: ");
            foreach (int num in numbers)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
            Console.WriteLine();

            numbers = BubbleSort(numbers);

            Console.Write("Result: ");
            foreach (int num in numbers)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
        }

        static int[] BubbleSort(int[] dataset)
        {
            for (var i = dataset.GetUpperBound(0); i != 0; i--)
            {
                for (var j = 0; j < i; j++)
                {
                    if (dataset[j] > dataset[j + 1])
                    {
                        (dataset[j], dataset[j + 1]) = (dataset[j + 1], dataset[j]);
                    }
                }
                Console.Write("Current State: ");
                foreach (int num in dataset)
                {
                    Console.Write($"{num} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            return dataset;
        }
    }
}
