﻿using System;
using System.Collections;

namespace hash_table
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();

            hashtable.Add("key1", 1);
            hashtable.Add("key2", 2);
            hashtable.Add("key3", 3);

            foreach (DictionaryEntry element in hashtable)
            {
                Console.Write($"{element.Key}={element.Value} ");
            }
            Console.WriteLine();

            Console.WriteLine($"Accessing a non-existent value won't throw an error: {hashtable["ht"]}");

            hashtable["key1"] = 10;
            foreach (DictionaryEntry element in hashtable)
            {
                Console.Write($"{element.Key}={element.Value} ");
            }
            Console.WriteLine();
        }
    }
}
