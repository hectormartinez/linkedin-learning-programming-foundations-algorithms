using System;
using System.Collections.Generic;

namespace counter
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = {
                "apple",
                "pear",
                "orange",
                "banana",
                "apple",
                "orange",
                "apple",
                "pear",
                "banana",
                "orange",
                "apple",
                "kiwi",
                "pear",
                "apple",
                "orange"
            };

            Dictionary<string, int> filter = new Dictionary<string, int>();
            foreach (string item in items)
            {
                if (filter.ContainsKey(item))
                {
                    filter[item] += 1;
                }
                else
                {
                    filter[item] = 1;
                }
            }

            foreach (KeyValuePair<string, int> item in filter)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
        }
    }
}
