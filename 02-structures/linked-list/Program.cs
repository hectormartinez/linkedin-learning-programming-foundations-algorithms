﻿using System;
using System.Collections.Generic;

namespace linked_list
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> intList = new LinkedList<int>();
            intList.AddFirst(38);
            intList.AddFirst(49);
            intList.AddFirst(13);
            intList.AddFirst(15);
            foreach (int item in intList)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

            Console.WriteLine($"Item count: {intList.Count}");
            Console.WriteLine($"Finding item: {intList.Find(13)}");
            Console.WriteLine($"Finding item: {intList.Find(78)}");

            intList.Remove(38);
            Console.WriteLine($"Item count: {intList.Count}");
        }
    }
}
