﻿using System;

namespace quick
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] items = { 20, 6, 8, 53, 56, 23, 87, 41, 49, 19 };
            Console.WriteLine(string.Join(" ", items));
            QuickSort(items, 0, items.GetUpperBound(0));
            Console.WriteLine(string.Join(" ", items));
        }

        static void QuickSort(int[] dataset, int first, int last)
        {
            if (first < last)
            {
                int pivotIndex = Partition(dataset, first, last);

                QuickSort(dataset, first, pivotIndex - 1);
                QuickSort(dataset, pivotIndex + 1, last);
            }
        }

        static int Partition(int[] dataset, int first, int last)
        {
            int pivot = dataset[first];
            int lower = first + 1;
            int upper = last;

            bool done = false;
            while (!done)
            {
                while (lower <= upper && dataset[lower] <= pivot)
                {
                    lower += 1;
                }

                while (upper >= lower && dataset[upper] >= pivot)
                {
                    upper -= 1;
                }

                if (upper < lower)
                {
                    done = true;
                }
                else
                {
                    (dataset[lower], dataset[upper]) = (dataset[upper], dataset[lower]);
                }
            }

            (dataset[first], dataset[upper]) = (dataset[upper], dataset[first]);
            return upper;
        }
    }
}
