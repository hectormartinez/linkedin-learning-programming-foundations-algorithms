﻿using System;

namespace gdc
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(CalculateGCD(60, 96));
            Console.WriteLine(CalculateGCD(20, 8));
            Console.WriteLine(CalculateGCD(20, 0));
        }

        static float CalculateGCD(float a, float b)
        {
            while (b != 0)
            {
                (a, b) = (b, a % b);
            }
            return a;
        }
    }
}
