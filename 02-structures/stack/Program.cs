﻿using System;
using System.Collections;

namespace stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);

            foreach (int element in stack)
            {
                Console.Write(element + " ");
            }
            Console.WriteLine();

            stack.Pop();
            foreach (int element in stack)
            {
                Console.Write(element + " ");
            }
            Console.WriteLine();
        }
    }
}
