﻿using System;

namespace recursion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"5 to the power of 3 is {Power(5, 3)}");
            Console.WriteLine($"1 to the power of 5 is {Power(1, 5)}");
            Console.WriteLine($"4! is {Factorial(4)}");
            Console.WriteLine($"0! is {Factorial(0)}");
        }

        static int Power(int num, int power)
        {
            if (power == 0)
            {
                return 1;
            }
            return num * Power(num, power - 1);
        }

        static int Factorial(int num)
        {
            if (num == 0)
            {
                return 1;
            }
            return num * Factorial(num - 1);
        }
    }
}
