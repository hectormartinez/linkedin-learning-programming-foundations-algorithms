using System;

namespace issorted
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sorted = { 6, 8, 19, 20, 23, 41, 49, 53, 56, 87 };
            int[] unsorted = { 6, 20, 8, 19, 56, 23, 87, 41, 49, 53 };

            Console.WriteLine(IsSorted(sorted));
            Console.WriteLine(IsSorted(unsorted));
        }

        static bool IsSorted(int[] list)
        {
            for (int i = 0; i < list.GetUpperBound(0); i++)
            {
                if (list[i] >= list[i + 1])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
