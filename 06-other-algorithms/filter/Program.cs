using System;
using System.Collections;
using System.Collections.Generic;

namespace filter
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = {
                "apple",
                "pear",
                "orange",
                "banana",
                "apple",
                "orange",
                "apple",
                "pear",
                "banana",
                "orange",
                "apple",
                "kiwi",
                "pear",
                "apple",
                "orange"
            };

            Hashtable filter = new Hashtable();
            foreach (string item in items)
            {
                filter[item] = 0;
            }

            foreach (string key in filter.Keys)
            {
                Console.Write($"{key} ");
            }
            Console.WriteLine();
        }
    }
}
