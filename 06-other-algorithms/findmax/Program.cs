using System;

namespace findmax
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] items = { 6, 20, 8, 19, 56, 23, 87, 41, 49, 53 };

            Console.WriteLine(FindMax(items));
        }

        static int FindMax(int[] list)
        {
            if (list.Length == 1)
            {
                return list[0];
            }

            int first = list[0];
            int second = FindMax(list[1..]);

            if (first > second)
            {
                return first;
            }
            else
            {
                return second;
            }
        }
    }
}
