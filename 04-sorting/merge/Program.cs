﻿using System;

namespace merge
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] items = { 6, 20, 8, 19, 56, 23, 87, 41, 49, 53 };
            Console.WriteLine($"Original: {string.Join(" ", items)}");
            MergeSort(items);
            Console.WriteLine($"Sorted: {string.Join(" ", items)}");
        }

        static void MergeSort(int[] dataset)
        {
            if (dataset.Length > 1)
            {
                int mid_index = dataset.Length / 2;
                int[] left = dataset[..mid_index];
                int[] right = dataset[mid_index..];
                // Console.WriteLine(string.Join(" ", dataset[..mid_index]));
                // Console.WriteLine(string.Join(" ", dataset[mid_index..]));

                MergeSort(left);
                MergeSort(right);

                int i = 0;
                int j = 0;
                int k = 0;

                while (i < left.Length && j < right.Length)
                {
                    if (left[i] < right[j])
                    {
                        dataset[k] = left[i];
                        i += 1;
                    }
                    else
                    {
                        dataset[k] = right[j];
                        j += 1;
                    }
                    k += 1;
                }

                while (i < left.Length)
                {
                    dataset[k] = left[i];
                    i += 1;
                    k += 1;
                }

                while (j < right.Length)
                {
                    dataset[k] = right[j];
                    j += 1;
                    k += 1;
                }



            }

        }
    }
}
