﻿using System;
using System.Collections;

namespace queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue queue = new Queue();

            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);

            foreach (int element in queue)
            {
                Console.Write($"{element} ");
            }
            Console.WriteLine();

            Console.WriteLine(queue.Dequeue());
            foreach (int element in queue)
            {
                Console.Write($"{element} ");
            }
            Console.WriteLine();
        }
    }
}
